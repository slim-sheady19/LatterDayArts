// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnVR.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/SceneComponent.h"
#include "MotionControllerVR.h"


// Sets default values
APlayerPawnVR::APlayerPawnVR()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	VRCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("VRCamera"));
	VRCamera->SetupAttachment(Root);

	/*Capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CharacterCapsule"));
	Capsule->SetupAttachment(GetRootComponent());
	Capsule->InitCapsuleSize(50.f, 100.f);*/
}

// Called when the game starts or when spawned
void APlayerPawnVR::BeginPlay()
{
	Super::BeginPlay();
	
	UHeadMountedDisplayFunctionLibrary::SetTrackingOrigin(EHMDTrackingOrigin::Floor);

	FAttachmentTransformRules rules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, true);

	RController = GetWorld()->SpawnActor<AMotionControllerVR>();
	RController->SetOwner(this);
	RController->AttachToComponent(Root, rules);
	RController->hand = EControllerHand::Right;

	LController = GetWorld()->SpawnActor<AMotionControllerVR>();
	LController->SetOwner(this);
	LController->AttachToComponent(Root, rules);
	LController->hand = EControllerHand::Left;
}

// Called every frame
void APlayerPawnVR::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnVR::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &APlayerPawnVR::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &APlayerPawnVR::MoveRight);

}


void APlayerPawnVR::MoveForward(float throttle)
{
	AddMovementInput(throttle * VRCamera->GetForwardVector());
}

void APlayerPawnVR::MoveRight(float throttle)
{
	AddMovementInput(throttle * VRCamera->GetRightVector());
}