// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MotionControllerVR.generated.h"

UCLASS()
class LATTERDAYARTS_API AMotionControllerVR : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere)
	class USceneComponent* Root;

	UPROPERTY(VisibleAnywhere)
	class UMotionControllerComponent* MotionController;

	UPROPERTY(VisibleAnywhere)
	class USkeletalMeshComponent* HandMesh;

	
public:	
	// Sets default values for this actor's properties
	AMotionControllerVR();

	EControllerHand hand;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
