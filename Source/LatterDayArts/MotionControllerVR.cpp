// Fill out your copyright notice in the Description page of Project Settings.


#include "MotionControllerVR.h"
#include "MotionControllerComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SceneComponent.h"
#include "CoreUObject/Public/UObject/ConstructorHelpers.h" //https://docs.unrealengine.com/4.26/en-US/API/Runtime/CoreUObject/UObject/ConstructorHelpers/ let us grab objects from content browser
#include "XRMotionControllerBase.h"

// Sets default values
AMotionControllerVR::AMotionControllerVR()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MotionController"));

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkelMesh(TEXT("/Game/VR_Assets/VirtualReality/Mannequin/Character/Mesh/MannequinHand_Right"));

	HandMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("HandMesh"));
	HandMesh->SetupAttachment(MotionController);
	HandMesh->SetSkeletalMesh(SkelMesh.Object);
	HandMesh->SetRelativeLocation(FVector(-10.0f, 0.0f, 0.0f));

}

// Called when the game starts or when spawned
void AMotionControllerVR::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMotionControllerVR::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FAttachmentTransformRules rules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, true);

	if (hand == EControllerHand::Right)
	{
		MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
		MotionController->AttachToComponent(this->RootComponent, rules);

		HandMesh->SetRelativeRotation(FRotator(0.0f, 0.0f, 90.0f));

		hand = EControllerHand::AnyHand;
	}
	else if (hand == EControllerHand::Left)
	{
		MotionController->MotionSource = FXRMotionControllerBase::LeftHandSourceId;
		MotionController->AttachToComponent(this->RootComponent, rules);

		HandMesh->SetRelativeRotation(FRotator(0.0f, 0.0f, 90.0f));
		HandMesh->SetRelativeScale3D(FVector(1.0f, 1.0f, -1.0f));

		hand = EControllerHand::AnyHand;
	}
}

