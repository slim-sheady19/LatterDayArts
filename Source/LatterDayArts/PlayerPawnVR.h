// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnVR.generated.h"

UCLASS()
class LATTERDAYARTS_API APlayerPawnVR : public APawn
{
	GENERATED_BODY()

	

	UPROPERTY(VisibleAnywhere)
	class USceneComponent* Root;

	//UPROPERTY(VisibleAnywhere)
	//class USceneComponent* VROrigin;


public:
	// Sets default values for this pawn's properties
	APlayerPawnVR();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UCameraComponent* VRCamera;

	UPROPERTY(VisibleAnywhere)
	class AMotionControllerVR* RController;

	UPROPERTY(VisibleAnywhere)
	class AMotionControllerVR* LController;

	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	//class UCapsuleComponent* Capsule;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float throttle);
	void MoveRight(float throttle);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


};
